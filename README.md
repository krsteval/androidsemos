![Screenshot_25.png](https://bitbucket.org/repo/XX59KA5/images/220914493-Screenshot_25.png)
# Упатство #

Поголем дел од потешките вежби се прикачени на следново репозитори

### Што има на следново репозитори ? ###

Следново репозитори служи за прикачување на некои од вежбите кои ги решаваме или задаваме на часовите во [**Семос Едукација**](http://semosedu.com.mk). По барање на студентите, се со цел да избегнуваме комуникација преку меил, усб.., а со самото тоа ќе послужи поголем дел од слушателите да се стекнаат со искуство со ГИТ, како и да пробат самите да верзионираат код кој би бил еден мал тренинг, искуство кое понатаму би им послужило.

* Стартен проект на целото репо е гранката мастер(branch: master) 
* Вежбата е листа од слушателите(дами дата податоци) по курсот Андроид во „Семос Едукација“ во контрола како ListView/GridView со помош на со сопствен(Custom) адаптер. 
* Version 1.0
* Наредните вежби ќе бидат прикачувани во различни гранки ([branches](https://bitbucket.org/krsteval/androidsemos/branches/)) 
* ###Превземање/Преглед на сорс кодот од сите вежби на [следниов линк](https://bitbucket.org/krsteval/androidsemos/downloads/?tab=branches)

* [Learn Markdown](http://semosedu.com.mk/default.aspx?mid=39&pid=31&kategorijaId=390&Lan=MK)

### Вежба 1 (**branch: master**) - Grid/List View  [превземи](https://bitbucket.org/krsteval/androidsemos/get/master.zip)

* Приказ на елементи во листа
* Креирање на адаптер
* Детали за корисник
* Евенти од лист ајтемите

### Вежба 2 (**branch: ActionBars**) - ActionBars View  [превземи](https://bitbucket.org/krsteval/androidsemos/get/ActionBars.zip)

* Приказ на попуп листа со клик на копче
* Креирање на ActionBar со он лонг клик на копче и манипулирање со мени итемите
* Клик на елементи од менито

### Вежба 3 (**branch: Fragments**) - Fragment  [превземи](https://bitbucket.org/krsteval/androidsemos/get/Fragments.zip)

* Приказ на попуп листа со клик на копче
* Креирање на ActionBar со он лонг клик на копче и манипулирање со мени итемите
* Клик на елементи од менито

### Вежба 4 (**branch: CustomComponentsAndStyles**) - Custom components, Styles, Themes..  [превземи](https://bitbucket.org/krsteval/androidsemos/get/CustomComponentsAndStyles.zip)

* Приказ на поинаков дизајн на копче
* Креирање на сенки, бордер, градиент на копче и манипулирање со атрибутите од рес директориумот
* Креирање на надворешни дизајни, и наследување на истите
* Креирање на теми
* Динамична компонента преку код
* Запознавање со анимации при евенти
* Креирање на анимации, запознавање со соодветнте директориумите

### Вежба 5 (**branch: WebView_VideoView**) - WebView и VideoView компоненти### [превземи](https://bitbucket.org/krsteval/androidsemos/get/WebView_VideoView.zip)

* Приказ на веб содржина во самата апликација
* Пополнување на едит поле до одредена веб страна и приказ/вчитување доколку е валидно во самата апликација
* Приказ на видео и аудио содржина стримана преку УРЛ и приказ во апликација
* Специфични формати на аудио и видео
* Додавање на надворешна библиотека ( додавањето се прави во **build.gradle** фајлот)
* Директен линк до библиотеката за видео контролата (compile'com.linsea:universalvideoview:1.1.0@aar')
* Опис до библиотеката ([линк](https://github.com/linsea/UniversalVideoView))
* Ваквите контроли задолжително бараат одредени пермисии, една од нив е „Интернет“


### Вежба 6 (**branch: SlideTabs**) - slide tabs with fragments ### [превземи](https://bitbucket.org/krsteval/androidsemos/get/SlideTabs.zip)

* Приказ на содржина во фрагменти, вјуа вчитувајки ги во едно активити
* ViewPager компонента
* Кастимизиран адаптер за манипулација со ViewPager, и менадзирање на фрагментите 
* TabLayout компонента
* За да се користат најновите дизајни, како што во овој случај е TabLayout компонентата потребна е библиотеката (compile 'com.android.support:design:25.3.1' ) ---( 25.3.1 e мојата верзија која ја имам инсталирано локално вашата можеби да е поголема или помала соодветно, т.е бројчето немора да е исто. Ако сакате да е исто, а ја немате ќе ви јави порака да ја инсталирате)


### Вежба 7 (**branch: DataSQL_Lite **) - SQLLite зачување на податоци ### [превземи](https://bitbucket.org/krsteval/androidsemos/get/SQLite_AllFunction.zip)

####Shared Preference – зачувување на примитивни податоци во Андроид.  ####
Ако поставите нарачка од онлајн продавница, бројот на вашата нарачка е примитивен затоа што тоа е Long или Double, не можете директно да ја зачувате вашата нарачка.

Манипулација со SharedPreference: 

**//      Читање**


```
#!Java

ime_Promenliva= PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("KLUCOT_ZA_PROMENLIVATA", ime_Promenliva);
```
		
		
		
**//      Сетирање** 


```
#!Java

PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("KLUCOT_ZA_PROMENLIVATA", ime_Promenliva).commit();
```
### Примерите со управување со локална база на податоци: ####
* SQLite инициална конфигурација, испишување во конзола [**превземи**](https://bitbucket.org/krsteval/androidsemos/get/SQLite.zip)
* SQLite надополнети фунционалности, целиот примерот од вежбите е надограден на база на претходниот погоре[**превземи**](https://bitbucket.org/krsteval/androidsemos/get/SQLite_AllFunction.zip)                    
      
### Вежба 8 (**branch: Notifications**) - нотификации примери ### [превземи](https://bitbucket.org/krsteval/androidsemos/get/Notifications.zip)

* Приказ на нотификација/известување
* Динамичко креирање на нотификации
* Сетирање на различни акции, како звук, светкање, вибрации при добивање, креирње на нотификација
* Сопствен дизајн за една нотификација
     
### Вежба 9 (**branch: AsyncTask**) - парсирање на јсон преку веб сервиси ### [превземи](https://bitbucket.org/krsteval/androidsemos/get/AsyncTask.zip)

* Имплементација на AsyncTask
* Запознавање на методите при имплементација на класа која наследува од AsyncTask
* GET повици до сервис [GetSemosCourses](http://entirelogic.noip.me:8087/api/default/GetSemosCourses)
* Парсирање на JSONArray и JSONObject објекти
* Полнење на ArrayListi со сопствен адаптер

### Вежба 10 (**branch: AsyncTaskPicasso**) - парсирање на јсон преку веб сервиси ### [превземи](https://bitbucket.org/krsteval/androidsemos/get/AsyncTaskPicasso.zip)
* Полнење на на ArrayListi со сопствен адаптер
* Приказ на асинхроно лоадирање на сликите со Picasso библиотека

### Вежба 11 (**branch: AsyncTaskPost**) - веб сервиси додавање нови итеми на сервер ### [превземи](https://bitbucket.org/krsteval/androidsemos/get/AsyncTaskPost.zip)

### Вежба 12 (**branch: AsyncTask_NBA_API**) - парсирање на јсон преку веб сервис на NBA.com ### [превземи](https://bitbucket.org/krsteval/androidsemos/get/AsyncTask_NBA_API.zip)


### Вежба 13 (**branch: Mapa**) - Мапа  ### [превземи](https://bitbucket.org/krsteval/androidsemos/get/Mapa.zip)


### Who do I talk to? ###

* Repo owner or admin - **Krste Moskov**
* Skype:    krste_king
* E-mail:   krstemoskov@gmail.com || krstemoskov@yahoo.com || krstemoskov@hotmail.com 
* LinkedIn: [direct link](linkedin.com/in/krste-moskov-7a1296b0)
* For personal use
* Other community or team contact