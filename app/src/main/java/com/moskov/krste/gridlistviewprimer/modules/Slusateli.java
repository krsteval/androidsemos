package com.moskov.krste.gridlistviewprimer.modules;

/**
 * Created by Krste on 6/10/2017.
 */

public class Slusateli {

    public String ime;
    public String prezime;
    public Integer telefon;
    public Integer godini;

    public Slusateli(String ime, String prezime, Integer godini) {
        this.ime = ime;
        this.prezime = prezime;
        this.godini = godini;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public Integer getTelefon() {
        return telefon;
    }

    public void setTelefon(Integer telefon) {
        this.telefon = telefon;
    }

    public Integer getGodini() {
        return godini;
    }

    public void setGodini(Integer godini) {
        this.godini = godini;
    }
}
