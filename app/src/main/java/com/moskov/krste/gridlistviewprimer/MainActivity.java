package com.moskov.krste.gridlistviewprimer;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.moskov.krste.gridlistviewprimer.adapters.MySimpleAdapter;
import com.moskov.krste.gridlistviewprimer.modules.Slusateli;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<Slusateli> slusateliList;
    MySimpleAdapter adapter;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponent();

        pecatiIzlez("Клик на листа и задржи на елемент од листа за дополнителна функционалност!");

    }

    private void initComponent() {

        slusateliList = new ArrayList<>();
        slusateliList.add(new Slusateli("Марјан", "Митровски", 1));
        slusateliList.add(new Slusateli("Горан", "Воиновски", 2));
        slusateliList.add(new Slusateli("Дамјан", "Милошевски", 3));
        slusateliList.add(new Slusateli("Марјана", "Миладинова", 4));
        slusateliList.add(new Slusateli("Борис", "Јордановски", 5));
        slusateliList.add(new Slusateli("Андреј", "Манчевски", 6));
        slusateliList.add(new Slusateli("Драган", "Димитровски", 7));
        slusateliList.add(new Slusateli("Марко", "Гелев", 8));
        slusateliList.add(new Slusateli("Фани", "Лазовска", 9));
        slusateliList.add(new Slusateli("Андреј", "Арсовски", 10));
        slusateliList.add(new Slusateli("Стефанија", "Ристоманова", 11));
        slusateliList.add(new Slusateli("Крсте", "Москов", 12));

        listView = (ListView) findViewById(R.id.slusateli_list);
        adapter= new MySimpleAdapter(MainActivity.this, slusateliList);

        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                pecatiIzlez(slusateliList.get(position).getIme());

            }
        });

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                slusateliList.remove(position);
                adapter.notifyDataSetChanged();
                return false;
            }
        });
    }

    private void pecatiIzlez(String msg) {

        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_SHORT).show();
    }
}
