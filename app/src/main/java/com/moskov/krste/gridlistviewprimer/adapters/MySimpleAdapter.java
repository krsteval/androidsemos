package com.moskov.krste.gridlistviewprimer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.moskov.krste.gridlistviewprimer.R;
import com.moskov.krste.gridlistviewprimer.modules.Slusateli;

import java.util.ArrayList;

/**
 * Created by Krste on 6/10/2017.
 */

public class MySimpleAdapter extends BaseAdapter {

    public ArrayList<Slusateli> slusatelisList;
    public Context context;

    public MySimpleAdapter(Context context, ArrayList<Slusateli> slusatelisList) {

        this.context = context;
        this.slusatelisList = slusatelisList;
    }

    @Override
    public int getCount() {
        return slusatelisList.size();
    }

    @Override
    public Object getItem(int position) {
        return slusatelisList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.single_slusatel_item, parent, false);
        TextView textViewName= (TextView) rowView.findViewById(R.id.txtUserName);
        TextView textViewLastName= (TextView) rowView.findViewById(R.id.txtUserLastName);
        TextView textViewAge = (TextView) rowView.findViewById(R.id.txtUserAge);

        textViewName.setText(slusatelisList.get(position).getIme().toString());
        textViewLastName.setText(slusatelisList.get(position).getPrezime().toString());
        textViewAge.setText(slusatelisList.get(position).getGodini().toString());
        return rowView;
    }
}
